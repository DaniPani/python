import experimental_save as ec
import numpy as np
   #NeuralNetwork(n_input, [n_neuroni], n_output)
neural_network = ec.NeuralNetwork(3,[4], 1)


#print ("Stage 1) Random starting synaptic weights: ")
#neural_network.print_weights()
#If output > 2, .T is not necessary
X = np.array([[0, 0, 1], [1, 1, 1], [1, 0, 1], [0, 1, 1]])
y = np.array([[0, 1, 1, 0]]).T

neural_network.fit(X,y, 10000)

neural_network.print_weights()

neural_network.test(np.array([[1, 0, 0]]), [1], True, ['zero', 'uno'])

neural_network.graph(errors=True)
