import numpy as np

#derivata
def deriv(x):
    return x*(1-x)

#sigmoide con np.exp(-x) = e (costante di Eulero: 2,71828)^x
def sigmoid(x):
    return 1/(1+np.exp(-x))

def predict(inputs):
    output_from_layer1 = sigmoid(np.dot(inputs, syn0)+b0)
    output_from_layer2 = sigmoid(np.dot(output_from_layer1, syn1)+b1)
    return output_from_layer1, output_from_layer2
    
X = np.array([[0, 0, 2], [0, 2, 2], [2, 0, 2], [0, 2, 0], [2, 0, 0], [2, 2, 2], [0, 0, 0]])
                
y = np.array([[0, 1, 1, 1, 1, 0, 0]]).T

np.random.seed(1)

# randomly initialize our weights with mean (numero di input, numero di neuroni)
syn0 = np.random.uniform(size=(3,4))
b0= np.random.uniform(size=(1,4))
syn1 = np.random.uniform(size=(4,1)) 
b1 = np.random.uniform(size=(1,1))
lr = 1

for j in np.arange(100000):

	# Feed forward through layers 0, 1, and 2
    l0 = X
    l1 = sigmoid(np.dot(l0,syn0)+b0)
    l2 = sigmoid(np.dot(l1,syn1)+b1)

    # how much did we miss the target value?
    l2_error = y - l2
    
    if (j% 10000) == 0:
        print ("Error:" + str(np.mean(np.abs(l2_error))))
        
    # in what direction is the target value?
    # were we really sure? if so, don't change too much.
    l2_delta = l2_error*deriv(l2)

    # how much did each l1 value contribute to the l2 error (according to the weights)?
    l1_error = np.dot(l2_delta, syn1.T)
    
    # in what direction is the target l1?
    # were we really sure? if so, don't change too much.
    l1_delta = l1_error * deriv(l1)

    syn1 += np.dot(l1.T,l2_delta) * lr
    b1 += np.sum(l2_delta, axis=0,keepdims=True) *lr
    syn0 += np.dot(l0.T,l1_delta) *lr
    b0 += np.sum(l1_delta, axis=0,keepdims=True) *lr

print ("Stage 3) Considering a new situation [1, 1, 0] -> ?: ")
hidden_state, output = predict(np.array([1, 1, 0]))
print(output)
