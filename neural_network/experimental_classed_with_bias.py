import numpy as np
import sys
import neural_draw as nd
from matplotlib import pyplot
np.random.seed(1)

class NeuronLayer():
    def __init__(self, number_of_neurons, number_of_inputs_per_neuron):
        self.weights = np.random.uniform(size=(number_of_inputs_per_neuron, number_of_neurons))
        self.bias = np.random.uniform(size=(1, number_of_neurons))
        self.nn = number_of_neurons
        self.ni = number_of_inputs_per_neuron
        self.list_error = []
        self.list_weight = []


class NeuralNetwork():
    def __init__(self, n_in , neurons, n_out, lr):
        #Computetation
        self.learning_rate = lr
        self.learning_curve = []
        self.layers = []
        self.layers.append(NeuronLayer(neurons[0], n_in))
        neurons.pop(0)
        for n in neurons:
            self.layers.append(NeuronLayer(n, self.layers[-1].nn))
        self.layers.append(NeuronLayer(n_out, self.layers[-1].nn))
        self.prepare_neural_draw(n_in)

    
    def prepare_neural_draw(self, n_in):
        #It builds the plot of the neural draw
        self.draw_neural_network = nd.NeuralNetwork()
        self.draw_neural_network.add_layer(n_in)
        for i in self.layers:
            self.draw_neural_network.add_layer(i.nn)
        self.draw_neural_network.draw()

    def __sigmoid(self, x):
        return 1.0 / (1.0 + np.exp(-x))

    def __deriv(self, x):
        return x * (1.0 - x)

    def fit(self, X, y, number_of_training_iterations):
        self.epoch = number_of_training_iterations
        #Inizialized ProgressBar
        z = 0
        progress(z, self.epoch)

        for iteration in np.arange(self.epoch):
            z += 1
            progress(z, self.epoch)

            l = self.think(X)
            l.reverse() #max -1 

            self.layers.reverse() #self.layers: 1,2,3 --> #max -1

            #Last Layer (Output layer)
            l_delta = [] #max - 1
            lmax_error = y - l[0]
            self.learning_curve.append(1/np.mean(np.abs(lmax_error)))
            lmax_delta = lmax_error*self.__deriv(l[0])
            self.layers[-1].list_error.append( np.mean(np.abs(lmax_error)))
            l_delta.append(lmax_delta)

            #Second Last Layer
            lmax2_error = np.dot(lmax_delta, self.layers[0].weights.T)
            lmax2_delta = lmax2_error*self.__deriv(l[1])
            self.layers[-2].list_error.append( np.mean(np.abs(lmax2_error)))
            l_delta.append(lmax2_delta) #max - 1

            self.error(lmax_error, z)

            ce = l.copy()
            l.pop(0)
            l.pop(0)
            for i in np.arange(len(l)):
                error = np.dot(l_delta[len(l_delta)-1], self.layers[i+1].weights.T)
                delta = error * self.__deriv(l[i])
                self.layers[i].list_error.append(np.mean(np.abs(error)))
                l_delta.append(delta)
            

            # self.layers max - 1
            self.layers[0].weights += np.dot(ce[1].T,lmax_delta) *self.learning_rate
            self.layers[0].bias += np.sum(lmax_delta, axis=0,keepdims=True) *self.learning_rate
            if (len(self.layers)>2):
                self.layers[1].weights += np.dot(ce[2].T,lmax2_delta) *self.learning_rate
                self.layers[1].bias += np.sum(lmax2_delta)* self.learning_rate
                self.layers[-1].weights += np.dot(X.T, l_delta[-1]) * self.learning_rate
                self.layers[-1].bias += np.sum(l_delta[-1]) * self.learning_rate
            l_delta.pop(0)
            l_delta.pop(0)
            for i in np.arange(len(l_delta)-1):
                self.layers[i+2].weights += np.dot(ce[i+3].T, l_delta[i]) *self.learning_rate
                self.layers[i+2].bias += np.sum(l_delta[i]) *self.learning_rate
            self.layers.reverse()
    
    # The neural network thinks.
    def think(self, inputs):

        outputs = []
        outputs.append(self.__sigmoid(np.dot(inputs,self.layers[0].weights))+ self.layers[0].bias)
        l_before = outputs[0]
        for i in np.arange(len(self.layers)-1):
            l = self.__sigmoid(np.dot(l_before,self.layers[i+1].weights)+ self.layers[i+1].bias)
            outputs.append(l)
            l_before = l
        return outputs

    def test(self, inputs, expected, report):
        io = []
        false = []
        true = []
        average = []
        o = 0
        for i in inputs:
            outputs = self.think(np.array([i]))
            io.append([outputs[-1], expected[o]])
            average.append(1-expected[o] - outputs[-1][0])
            if np.mean(np.abs(expected[o] - outputs[-1][0]))> 0.5:
                false.append(i)
            else:
                true.append(i)
            o += 1
        if(report):
            print('\n')
            print('Predicted            Expected')
            io = np.array(io)
            print(io.reshape(len(io)*2,1))
            print(self)
            print('Report:')
            print('precision:   recall:')
            print(np.mean(np.abs(average)) , len(true)/(len(false)+len(true)))
        else:
            print('\n')
            print(io)

    # The neural network prints its weights
    def print_weights(self):
        iteration = 0
        for i in self.layers:
            print('Layer ' + str(iteration+1) +  ' (' + str(i.nn) + ' neurons, each with ' + str(i.ni) + ' inputs): ')
            print(i.weights)
            iteration +=1 
    
    def graph(self, errors):
        if(errors):
            pyplot.subplots_adjust(hspace=1)
            row = (len(self.layers)+3)/2  
            pyplot.subplot(row,2, 1 )      
            self.draw_neural_network.draw()
            a=0
            for i in np.arange(len(self.layers)):
                a = 2+i
                pyplot.subplot(row,2, 2+i)  
                pyplot.title( 'Error of ' + str(i+1)+ "° Layer (" + str(self.layers[i].nn) + 'N) in %')
                pyplot.plot(np.arange(self.epoch),np.dot(self.layers[i].list_error, 100), 'r')
            pyplot.subplot(row,2,a+1)
            pyplot.plot(np.arange(self.epoch), self.learning_curve, 'g')
            pyplot.title('Learning Curve')
        pyplot.show()
    
    def error(self, lmax_error, z):
        if (self.epoch% (self.epoch/100)) == 0:
            progress(z, self.epoch, ' Error =~ '+ str(np.mean(np.abs(lmax_error))))
    

#Progress Bar
def progress(count, total, status=''):
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    bar = '=' * (filled_len) +'>' + '-' * (bar_len - filled_len)

    sys.stdout.write('[%s] %s%s ...%s\r' % (bar, percents, '%', status))
    sys.stdout.flush()

