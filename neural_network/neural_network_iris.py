import experimental_save as ec
import numpy as np
from sklearn import datasets, model_selection

iris = datasets.load_iris()
X_train, X_test, y_train, y_test = model_selection.train_test_split(iris.data[:100], iris.target[:100], test_size= 0.25)
   #NeuralNetwork(n_input, [n_neuroni], n_output)
neural_network = ec.NeuralNetwork(4,[6], 1)

neural_network.print_weights()
neural_network.fit(X_train, np.array([y_train]).T, 5000)

neural_network.test(X_test, np.array(y_test), True, ['I. setosa','I. versicolor'])


neural_network.graph(errors=True)
