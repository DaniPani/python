import experimental_save as ec
import numpy as np
import matplotlib.pyplot as plt
from sklearn import datasets, model_selection

digits = datasets.load_digits(2)

X_train, X_test, y_train, y_test = model_selection.train_test_split(digits.data[:100],digits.target[:100], test_size= 0.25)
   #NeuralNetwork(n_input, [n_neuroni], n_output)
neural_network = ec.NeuralNetwork(64,[15,6,2], 1)

print ("Stage 1) Random starting synaptic weights: ")
#neural_network.print_weights()
#If output > 2, .T is not necessary

neural_network.fit(X_train, np.array([y_train]).T, 500)

#print ("Stage 2) New synaptic weights after training: ")
#neural_network.print_weights()

#output = neural_network.test(X_test, np.array(y_test), True, ['Zero','Uno'])
#print (output)

#neural_network.graph(errors=True)

tester = neural_network.think(X_test)
y_pred = []
for i in tester[0][-1]:
    y_pred.append(np.around(i))

y_pred = list(map(int, y_pred))
print('ehehe')
print(y_pred)
images_and_predictions = list(zip(digits.images[y_pred], y_pred))
for index, (image, prediction) in enumerate(images_and_predictions[:16]):
    plt.subplot(4, 4 , index+1 )
    plt.axis('off')
    plt.imshow(image, cmap=plt.cm.gray_r, interpolation='nearest')
    plt.title('Prediction: %i' % prediction)

plt.show()