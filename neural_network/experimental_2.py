import numpy as np

#derivata
def deriv(x):
    return x*(1-x)

#sigmoide con np.exp(-x) = e (costante di Eulero: 2,71828)^x
def sigmoid(x):
    return 1/(1+np.exp(-x))

def predict(inputs):
    output_from_layer1 = sigmoid(np.dot(inputs, syn0))
    output_from_layer2 = sigmoid(np.dot(output_from_layer1, syn1))
    output_from_layer3 = sigmoid(np.dot(output_from_layer2, syn2))
    return output_from_layer1, output_from_layer2, output_from_layer3
    
X = np.array([[0, 0, 1], [0, 1, 1], [1, 0, 1], [0, 1, 0], [1, 0, 0], [1, 1, 1], [0, 0, 0]])
                
y = np.array([[0, 1, 1, 1, 1, 0, 0]]).T

np.random.seed(1)

# randomly initialize our weights with mean 0 (numero di input, numero di neuroni)
syn0 = 2*np.random.random((3,4)) - 1
syn1 = 2*np.random.random((4,5)) - 1
syn2 = 2*np.random.random((5,1)) - 1

for j in np.arange(5000):

	# Feed forward through layers 0, 1, and 2
    l0 = X
    l1 = sigmoid(np.dot(l0,syn0))
    l2 = sigmoid(np.dot(l1,syn1))
    l3 = sigmoid(np.dot(l2,syn2))

    # how much did we miss the target value?
    l3_error = y - l3
    
    if (j% 10000) == 0:
        print ("Error:" + str(np.mean(np.abs(l3_error))))
        
    # in what direction is the target value?
    # were we really sure? if so, don't change too much.
    l3_delta = l3_error*deriv(l3)

    # hw much did each l1 value contribute to the l2 error (according to the weights)?
    l2_error = np.dot(l3_delta, syn2.T)
    l2_delta = l2_error * deriv(l2)

    l1_error = np.dot(l2_delta, syn1.T)
    l1_delta = l1_error * deriv(l1)
    print(l1.T.shape)
    syn2 += np.dot(l2.T,l3_delta)
    syn1 += np.dot(l1.T,l2_delta)
    syn0 += np.dot(l0.T,l1_delta)

print ("Stage 3) Considering a new situation [1, 1, 0] -> ?: ")
hidden_state1, hidden_state2, output = predict(np.array([1, 1, 0]))
print(output)
