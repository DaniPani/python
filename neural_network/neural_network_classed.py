import numpy as np
np.random.seed(1)

class NeuronLayer():
    def __init__(self, number_of_neurons, number_of_inputs_per_neuron):
        self.weights = 2 * np.random.random((number_of_inputs_per_neuron, number_of_neurons)) - 1


class NeuralNetwork():
    def __init__(self, layer1, layer2, layer3):
        self.layer1 = layer1
        self.layer2 = layer2

    def __sigmoid(self, x):
        return 1 / (1 + np.exp(-x))

    def __deriv(self, x):
        return x * (1 - x)

    def fit(self, X, y, number_of_training_iterations):
        for iteration in np.arange(number_of_training_iterations):
            # Pass the training set through our neural network
            output_from_layer_1, output_from_layer_2 = self.think(X)

            # Calculate the error for layer 2 (The difference between the desired output
            # and the predicted output).
            layer2_error = y - output_from_layer_2
            layer2_delta = layer2_error * self.__deriv(output_from_layer_2)

            # Calculate the error for layer 1 (By looking at the weights in layer 1,
            # we can determine by how much layer 1 contributed to the error in layer 2).
            layer1_error = np.dot(layer2_delta, self.layer2.weights.T)
            layer1_delta = layer1_error * self.__deriv(output_from_layer_1)

            # Calculate how much to adjust the weights by
            layer1_adjustment = np.dot(X.T, layer1_delta)
            layer2_adjustment = np.dot(output_from_layer_1.T, layer2_delta)

            # Adjust the weights.
            self.layer1.weights += layer1_adjustment
            self.layer2.weights += layer2_adjustment

    # The neural network thinks.
    def think(self, inputs):
        output_from_layer1 = self.__sigmoid(np.dot(inputs, self.layer1.weights))
        output_from_layer2 = self.__sigmoid(np.dot(output_from_layer1, self.layer2.weights))
        return output_from_layer1, output_from_layer2

    # The neural network prints its weights
    def print_weights(self):
        print ("    Layer 1 (4 neurons, each with 3 inputs): ")
        print (self.layer1.weights)
        print ("    Layer 2 (1 neuron, with 4 inputs):")
        print (self.layer2.weights)

if __name__ == "__main__":

    # Create layer 1 (4 neurons, each with 3 inputs)
    layer1 = NeuronLayer(4, 3)

    # Create layer 2 (a single neuron with 4 inputs)
    layer2 = NeuronLayer(1, 4)

    # Combine the layers to create a neural network
    neural_network = NeuralNetwork(layer1, layer2)

    print ("Stage 1) Random starting synaptic weights: ")
    neural_network.print_weights()

    # The training set. We have 7 examples, each consisting of 3 input values
    # and 1 output value.
    X = np.array([[0, 0, 1], [0, 1, 1], [1, 0, 1], [0, 1, 0], [1, 0, 0], [1, 1, 1], [0, 0, 0]])
    y = np.array([[0, 1, 1, 1, 1, 0, 0]]).T

    # Train the neural network using the training set.
    # Do it 60,000 times and make small adjustments each time.
    neural_network.fit(X,y, 60000)

    print ("Stage 2) New synaptic weights after training: ")
    neural_network.print_weights()

    # Test the neural network with a new situation.
    print ("Stage 3) Considering a new situation [1, 1, 0] -> ?: ")
    hidden_state, output = neural_network.think(np.array([1, 1, 0]))
    print (output)