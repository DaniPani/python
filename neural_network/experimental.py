import numpy as np
import matplotlib.pyplot as plt

#derivata
def deriv(x):
    return x*(1-x)

#sigmoide con np.exp(-x) = e (costante di Eulero: 2,71828)^x
def sigmoid(x):
    return 1/(1+np.exp(-x))

def predict(inputs):
# Pass inputs through our neural network (our single neuron).
    return sigmoid(np.dot(inputs, weights))

    
X = np.array([[0, 0, 1], [0, 1, 1], [1, 0, 1], [0, 1, 0], [1, 0, 0], [1, 1, 1], [0, 0, 0]])
                
y = np.array([[0, 1, 1, 1, 1, 0, 0]]).T

# seed random numbers to make calculation
# deterministic (just a good practice)
np.random.seed(1)

# creiamo un array di 3 weights [-1, 1] (2*0-1 --> -1; 2*1-1 --> 1)
plot_data_y1 =  []
plot_data_y2 =  []
plot_data_y3 =  []
plot_error1 = []
plot_error2 = []
plot_error3 = []
plot_error4 = []
plot_error5 = []
plot_data_x = np.arange(10000)
weights = 2*np.random.random((3,1)) - 1

for iter in np.arange(1000000):

    # forward propagation
    l0 = X

    # moltiplica gli input con i weights e poi ne trova nell'intervallo di y [0; 1] in questo modo si è sicuri che gli output sono nell'intervello degli input
    l1 = sigmoid(np.dot(l0, weights))

    # how much did we miss?
    l1_error = y - l1

    # multiply how much we missed by the 
    # slope of the sigmoid at the values in l1
    l1_delta = l1_error * deriv(l1)
    # update weights
    weights +=np.dot(l0.T,l1_delta)
    plot_data_y1.append(weights[0][0])
    plot_data_y2.append(weights[1][0])
    plot_data_y3.append(weights[2][0])
    plot_error1.append(l1_error[0])
    plot_error2.append(l1_error[1])
    plot_error3.append(l1_error[2])
    plot_error4.append(l1_error[3])
    plot_error5.append(np.mean(np.abs(l1_error)))


    

print("Output After Training:")
print(l1)
print("New prediction:")
print(predict([1,0,0]))

f, (ax1, ax2) = plt.subplots(1, 2, sharey=True)
ax1.plot(plot_data_x, plot_data_y1, 'y--', plot_data_x, plot_data_y2, 'b--', plot_data_x, plot_data_y3, 'g--')
ax1.set_title('Weights')
ax2.plot(plot_data_x, plot_error1, 'r--',plot_data_x, plot_error2, 'r--',plot_data_x, plot_error3, 'r--',plot_data_x, plot_error4, 'r--', plot_data_x, plot_error5, 'b--')
ax2.set_title('Error')

plt.title('Result')

plt.show()