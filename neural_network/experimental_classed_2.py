import numpy as np
import sys
from matplotlib import pyplot
np.random.seed(1)

class NeuronLayer():
    def __init__(self, number_of_neurons, number_of_inputs_per_neuron):
        self.weights = np.random.uniform(size=(number_of_inputs_per_neuron, number_of_neurons))
        self.error = []
        self.delta_error = []
        self.ni= number_of_inputs_per_neuron
        self.nn =number_of_neurons
        self.output = []


class NeuralNetwork():
    def __init__(self, n_in , neurons, n_out, lr):
        #Computetation
        self.learning_rate = lr
        self.layers = []
        for i, neuron in enumerate(neurons):
            if i == 0:
                self.layers.append(NeuronLayer(neuron, n_in))
            if i == (len(neurons) -1):
                self.layers.append(NeuronLayer(n_out, self.layers[-1].nn))
            else:
                self.layers.append(NeuronLayer(neuron, self.layers[-1].nn))

    def __sigmoid(self, x):
        return 1 / (1 + np.exp(-x))

    def __deriv(self, x):
        return x * (1 - x)

    def fit(self, inputs, output, number_of_training_iterations):
        for (x, y) in zip(inputs, output):
            # Forward-pass training example into network (updates node output)
            self._forward_pass(x)
            # Create target output
                
             # Backward-pass error into network (updates node delta)
            self._backward_pass(y)
            # Update network weights (using updated node delta and node output)
            self._update_weights(x)

    # The neural network thinks.
    def _forward_pass(self, x):

        # Weighted sum of inputs with no bias term for our activation

        # Perform forward-pass through network and update node outputs
        inputs = x
        for layer in self.layers:
            layer.output = self.__sigmoid(np.dot(inputs, layer.weights))
            inputs = layer.output

        return inputs

    #
    # Backward-pass error into neural network
    # The loss function is assumed to be L2-error.
    # This updates: node['delta']
    #
    def _backward_pass(self, target):
        self.layers.reverse()
        for i, layer in enumerate(self.layers):
            if i == 0:
                layer.error = target - layer.output
            else:
                layer.error = np.dot(self.layers[i-1].delta_error, self.layers[i-1].weights.T)
            layer.delta_error = layer.error * self.__deriv(layer.output)
        self.layers.reverse()
    #
    # Update network weights with error
    # This updates: node['weights']
    #
    def _update_weights(self, x, l_rate=0.3):

        # Update weights forward layer by layer
        for i, layer in enumerate(self.layers):

            # Choose previous layer output to update current layer weights
            if i == 0:
                inputs = x
            else:
                print(self.layers[i-1].output)
                inputs = self.layers[i-1].output
            print(layer.delta_error)
            layer.weights += l_rate * np.dot(inputs, layer.delta_error.T)

    # The neural network prints its weights
    def print_weights(self):
        iteration = 0
        for i in self.layers:
            print('Layer ' + str(iteration+1) +  ' (' + str(i.nn) + ' neurons, each with ' + str(i.ni) + ' inputs): ')
            print(i.weights)
            iteration +=1 
    

#Progress Bar
def progress(count, total, status=''):
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    bar = '=' * (filled_len) +'>' + '-' * (bar_len - filled_len)

    sys.stdout.write('[%s] %s%s ...%s\r' % (bar, percents, '%', status))
    sys.stdout.flush()

