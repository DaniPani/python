import experimental_save as ec
from sklearn import model_selection
import numpy as np
from skimage import io, color
from skimage.transform import rescale, resize
import zipfile as zp
import matplotlib.pyplot as plt
import zipfile


Leo_faces = []
Leo_faces_grey = []
Leo_i = 0
with zipfile.ZipFile('C:/Users/danie/Google Drive/Python/Neural_network/leo_min.zip') as f:
    for item in f.namelist()[:10]:
        with f.open(item) as file:
            print('Aprendo Leo: ' + str(item))  
            image = io.imread(file)
            resized = rescale(image, 0.02)

            Leo_faces.append(resized)
            Leo_i +=1



for i in Leo_faces:
    grey = color.rgb2grey(i)

    Leo_faces_grey.append(grey.flatten())


Rachi_faces = []
Rachi_faces_grey = []
Rachi_i = 0
with zipfile.ZipFile('C:/Users/danie/Google Drive/Python/Neural_network/rachele_min.zip') as f:
    for item in f.namelist()[:10]:
        with f.open(item) as file:
            print('Aprendo Rachele: ' + str(item))
            image = io.imread(file)
            resized = rescale(image, 0.02)
            #plt.imshow(resized)
            #plt.show()
            Rachi_faces.append(resized)
            Rachi_i +=1



for i in Rachi_faces:
    grey = color.rgb2grey(i)
    Rachi_faces_grey.append(grey.flatten())

X_train, X_test, y_train, y_test = model_selection.train_test_split(np.array(Leo_faces_grey+ Rachi_faces_grey), np.array([0] * Leo_i+ [1]* Rachi_i),random_state=42)
#15,6,2
neural_network = ec.NeuralNetwork(4860,[15,6,2], 1)

neural_network.fit(X_train, np.array([y_train]).T, 10000)

neural_network.test(X_test, y_test, True, ['Leo','Rachele'])

