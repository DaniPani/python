import numpy as np
import sys
import neural_draw as nd
from matplotlib import pyplot
np.random.seed(1)

class NeuronLayer():
    def __init__(self, number_of_neurons, number_of_inputs_per_neuron):
        self.weights = 2 * np.random.random((number_of_inputs_per_neuron, number_of_neurons)) - 1
        self.nn = number_of_neurons
        self.ni = number_of_inputs_per_neuron
        self.list_error = []
        self.list_weight = []


class NeuralNetwork():
    def __init__(self, n_in , neurons, n_out):
        self.layers = []
        self.draw_neural_network = nd.NeuralNetwork()
        self.layers.append(NeuronLayer(neurons[0], n_in))
        neurons.remove(neurons[0])
        for n in neurons:
            self.layers.append(NeuronLayer(n, self.layers[len(self.layers)-1].nn))
        
        self.layers.append(NeuronLayer(n_out, self.layers[len(self.layers)-1].nn))
        self.draw_neural_network.add_layer(n_in)
        for i in self.layers:
            self.draw_neural_network.add_layer(i.nn)
    def __sigmoid(self, x):
        return 1 / (1 + np.exp(-x))

    def __deriv(self, x):
        return x * (1 - x)

    def fit(self, X, y, number_of_training_iterations):
        self.number_of_training_iterations = number_of_training_iterations
        z = 0
        for iteration in np.arange(number_of_training_iterations):
            z += 1
            # Pass the training set through our neural network
            l = self.think(X) #1,2,3,....
            l.reverse() #max - 1


            self.layers.reverse() #self.layers: 1,2,3 --> #max -1
            # Calculate the error for layer 2 (The difference between the desired output
            # and the predicted output).
    # were we really sure? if so, don't change too much.
            l_delta = [] #max - 1
            lmax_error = y - l[0]

            if (number_of_training_iterations% 100) == 0:
                progress(z, number_of_training_iterations, ' Error =~ '+ str(np.mean(np.abs(lmax_error))))
                #print("Error:" + str(np.mean(np.abs(lmax_error))))

            lmax_delta = lmax_error*self.__deriv(l[0])
            self.layers[len(self.layers)-1].list_error.append( np.mean(np.abs(lmax_error)))
            l_delta.append(lmax_delta)

            lmax2_error = np.dot(lmax_delta, self.layers[0].weights.T)
            lmax2_delta = lmax2_error*self.__deriv(l[1])
            self.layers[len(self.layers)-2].list_error.append( np.mean(np.abs(lmax2_error)))
            l_delta.append(lmax2_delta) #max - 1
            iterator = 1
            ce = l.copy()
            l.remove(l[0])
            l.remove(l[0])
            for i in np.arange(len(l)):
                error = np.dot(l_delta[len(l_delta)-1], self.layers[iterator].weights.T)
                delta = error * self.__deriv(l[i])
                self.layers[i].list_error.append(np.mean(np.abs(error)))
                l_delta.append(delta)
                iterator +=1
            

            # self.layers max - 1
            self.layers[0].weights += np.dot(ce[1].T,lmax_delta)
            if (len(self.layers)>2):
                self.layers[1].weights += np.dot(ce[2].T,lmax2_delta)
                self.layers[len(self.layers)-1].weights += np.dot(X.T, l_delta[len(l_delta)-1])
            l_delta.remove(l_delta[0])
            l_delta.remove(l_delta[0])
            for i in np.arange(len(l_delta)-1):
                self.layers[i+2].weights += np.dot(ce[i+3].T, l_delta[i])
            self.layers.reverse()
        self.draw_neural_network.draw()
    
    # The neural network thinks.
    def think(self, inputs):

        outputs = []
        outputs.append(self.__sigmoid(np.dot(inputs,self.layers[0].weights)))
        l_before = outputs[0]
        for i in np.arange(len(self.layers)-1):
            l = self.__sigmoid(np.dot(l_before,self.layers[i+1].weights))
            outputs.append(l)
            l_before = l
        return outputs

    def test(self, inputs, expected, report, label=list()):
        io = []
        false = []
        true = []
        average = []
        o = 0
        for i in inputs:
            outputs = self.think(np.array([i]))
            if len(label) == 0:
                io.append([outputs[-1], expected[o]])              
            else:
                io.append([outputs[-1], expected[o], label[int(np.around(outputs[-1]))]])
            average.append(1-expected[o] - outputs[-1][0])
            if np.mean(np.abs(expected[o] - outputs[-1][0]))> 0.5:
                false.append(i)
            else:
                true.append(i)
            o += 1
        if(report):
            print('\n')
            print('Predicted            Expected        Label')
            io = np.array(io)
            print(io)
            print(self)
            print('Report:')
            print('precision:   recall:')
            print(np.mean(np.abs(average)) , len(true)/(len(false)+len(true)))
        else:
            print('\n')
            print(io)

    # The neural network prints its weights
    def print_weights(self):
        iteration = 0
        for i in self.layers:
            print('Layer ' + str(iteration+1) +  ' (' + str(self.layers[iteration].nn) + ' neurons, each with ' + str(self.layers[iteration].ni) + ' inputs): ')
            print(self.layers[iteration].weights)
            iteration +=1 
    
    def graph(self, errors):
        if(errors):
            pyplot.subplots_adjust(hspace=1)
            row = (len(self.layers)+2)/2  
            pyplot.subplot(row,2, 1 )      
            self.draw_neural_network.draw()
            for i in np.arange(len(self.layers)):
                pyplot.subplot(row,2, 2+i)  
                pyplot.title( 'Error of ' + str(i+1)+ "° Layer (" + str(self.layers[i].nn) + 'N) in %')
                pyplot.plot(np.arange(self.number_of_training_iterations),np.dot(self.layers[i].list_error, 100), 'r')
        pyplot.show()

    


def progress(count, total, status=''):
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    bar = '=' * (filled_len-2) +'>' + '-' * (bar_len - filled_len-1)

    sys.stdout.write('[%s] %s%s ...%s\r' % (bar, percents, '%', status))
    sys.stdout.flush()

 