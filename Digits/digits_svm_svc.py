import matplotlib.pyplot as plt
from sklearn import datasets, svm, metrics, model_selection
digits = datasets.load_digits()


n_samples = len(digits.images)

classifier = svm.SVC(gamma=0.001)

X_train, X_test, y_train, y_test = model_selection.train_test_split(digits.data, digits.target, test_size=0.25)

classifier.fit(X_train, y_train)


y_true = y_test
y_pred = classifier.predict(X_test)
print(list(zip(digits.images[y_pred], y_pred)))

print("Classification report for classifier %s:\n%s\n"
      % (classifier, metrics.classification_report(y_true, y_pred)))
print("Confusion matrix:\n%s" % metrics.confusion_matrix(y_true, y_pred))

images_and_predictions = list(zip(digits.images[y_pred], y_pred))
for index, (image, prediction) in enumerate(images_and_predictions[:8]):
    plt.subplot(4, 4 , index+1 )
    plt.axis('off')
    plt.imshow(image, cmap=plt.cm.gray_r, interpolation='nearest')
    plt.title('Prediction: %i' % prediction)

plt.show()