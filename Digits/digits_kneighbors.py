'import matplotlib.pyplot as plt
from sklearn import datasets, neighbors, metrics, model_selection
digits = datasets.load_digits()


n_samples = len(digits.images)

knn = neighbors.KNeighborsClassifier(6)

X_train, X_test, y_train, y_test = model_selection.train_test_split(digits.data, digits.target, test_size=0.25)

knn.fit(X_train, y_train)


y_true = y_test
y_pred = knn.predict(X_test)

print("Classification report for classifier %s:\n%s\n"
      % (knn, metrics.classification_report(y_true, y_pred)))
print("Confusion matrix:\n%s" % metrics.confusion_matrix(y_true, y_pred))

images_and_predictions = list(zip(digits.images[y_pred], y_pred))
for index, (image, prediction) in enumerate(images_and_predictions[:8]):
    plt.subplot(4, 4 , index+1 )
    plt.axis('off')
    plt.imshow(image, cmap=plt.cm.gray_r, interpolation='nearest')
    plt.title('Prediction: %i' % prediction)

plt.show()