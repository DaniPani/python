# l'hashtag è il simbolo per iniziare un commento, invece '-->' l'output 
# per definire una variabile un nome e assegnarli un valore
a = 4
g = 't' # per assegnare il valore di una stringa basta utilizzare '' o ""
print(a == 4) # --> True, == (identity testing) è diverso da = (variable assignament)
# i simboli += o -= sono la forma contratta di
a = a + 1 # è uguale a
a += 1 
# per mostrare qualcosa nella console viene utilizzato la funzione Built-In 'print()'
print(a) # --> 6
f = [1,2,3,4] #si usano le parentesi quadrate per creare delle liste
f.append(5) # aggiunge 5 alla lista f
print(f[0]) # --> 1
print(f[1]) # --> 2
print(f[:2]) # --> [1,2,3] ritorna i valori nell'intervallo 
print(len(f)) # --> 5; ritorna la lunghezza di una lista
f.pop(1) print(f) # --> [1,3,4,5]; rimuove il l'elemento con quel index 
if a == 6:
    print('Oh Yessss') # questa parte del codice viene eseguita solo se a è uguale a 6
else:
    print('E anche capace di leggere') # e questa se quella prima non è vera
    
for i in f:
    print(i)
    # --> 1
    # --> 3
    # --> 4
    # --> 5
# tramite il comando import ... (as)/from ... import ... (as) possiamo importanti i "module" o "libraries" ovvero parti ci codici gia scritte
import random as rd # questo importa una libreria per usare serie di funzioni per creare numeri/lettere random con il nome rd
a = rd.random()
# come creare una funzione:
def nomedellafunzione(args): # args è quello che vogliamo passare all'interno della funzione quando viene invocata
    if args == 0:
        return True # return statement viene usato quando la funzione deve ritornare quale
    else:
        return 'Cats are beautiful'

if nomedellafunzione(0) == True:
    print('Un milione di protoni')



