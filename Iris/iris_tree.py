

from sklearn import datasets, model_selection, tree, metrics

iris = datasets.load_iris()

X_train, X_test, y_train, y_test = model_selection.train_test_split(iris.data, iris.target, test_size=0.25, random_state=42)

tr = tree.DecisionTreeClassifier()

tr.fit(X_train, y_train)

y_true = y_test
y_pred = tr.predict(X_test)


print(tr, metrics.classification_report(y_true, y_pred))




