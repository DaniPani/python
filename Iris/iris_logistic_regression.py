from sklearn import datasets, linear_model, model_selection, metrics

iris = datasets.load_iris()

X_train, X_test, y_train, y_test = model_selection.train_test_split(iris.data, iris.target, test_size=0.25) 
#con random_state = n (qualsiasi) la generazione dei test rimane uguale tra le sessioni

lg = linear_model.LogisticRegressionCV()
#prova a modificare i "vicini" da considerare in neighbors.KNeighborsClassifier(n)

lg.fit(X_train, y_train)

y_true = y_test
y_pred = lg.predict(X_test)


#print(iris.target_names[knn.predict([X_test[n]])])

print("Classification report for classifier %s:\n%s\n"
      % (lg, metrics.classification_report(y_true, y_pred)))
print("Confusion matrix:\n%s" % metrics.confusion_matrix(y_true, y_pred))

