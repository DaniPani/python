from sklearn import datasets, neighbors, model_selection, metrics

iris = datasets.load_iris()

X_train, X_test, y_train, y_test = .train_test_split(iris.data, iris.target, test_size=0.25)

marco_classfier = neighbors.KNeighborsClassifier(5)

marco_classfier.fit(X_train, y_train)

print(marco_classfier.predict(X_test))


y_true = y_test
y_pred = marco_classfier.predict(X_test)

#print(iris.target_names[knn.predict([X_test[n]])])

print("Classification report for classifier %s:\n%s\n"
      % (marco_classfier, metrics.classification_report(y_true, y_pred)))
print("Confusion matrix:\n%s" % metrics.confusion_matrix(y_true, y_pred))


